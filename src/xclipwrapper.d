module wst.xclipwrapper;

import std.stdio;
import std.process;
import std.string;


/// Exception thrown on if communicating with xclip fails.
@safe pure nothrow class XclipInterfaceError: Exception
{
	this()
	{
		super("Cannot access xclip!");
	}
}

/// Exception thrown on if xclip is not installed.
@safe pure nothrow class XclipNotInstalled: Exception
{
	this()
	{
		super("!!Xclip is not installed!!");
	}
}

/++
	Test if xclip is installed.
	Returns: true if xclip is installed
++/
@safe bool isXclipInstalled()
{
	auto comp = execute(["which", "xclip"]);
	if (comp.status == 0) return true;
	return false;
}
///
unittest
{
	assert (isXclipInstalled());
}

/++
	Get the contents of the clipboard.
	Returns: string containing contents of the clipboard
	Throws: XclipInterfaceError if xclip returns non-zero
++/
@safe string getClipboard()
{
	auto clipres = execute(["xclip", "-out"]);
	if (clipres.status) throw new XclipInterfaceError;
	return clipres.output.strip;
}
///
unittest
{
	assert (isXclipInstalled());

	// We need to make sure something is actually in the clipboard first
	setClipboard("test string");
	assert (getClipboard() == "test string");
}

/++
	Set the contents of the clipboard to `newclip`.
	Throws: XclipInterfaceError if xclip returns non-zero
++/
@safe void setClipboard(string newclip)
{
	auto clippid = pipeProcess(["xclip", "-in"]);
	clippid.stdin.write(newclip);
	clippid.stdin.flush();
	clippid.stdin.close();
	if (wait(clippid.pid) != 0) throw new XclipInterfaceError;
}
///
unittest
{
	assert (isXclipInstalled());
	setClipboard("test string");
}

