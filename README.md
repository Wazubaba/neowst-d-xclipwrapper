WST - D branch: libxclipwrapper
===========================

This module provides an interface via D's subproc functionality
to make the required calls to interface with xclip.

Obviously, one would require xclip installed to use this. :D

# 30-mile-high overview
The first step: test for xclip installation via `isXclipInstalled()`.
If not, you will need to find another way. If so, then you can just
call `getClipboard()` and `setClipboard()` to interact.

